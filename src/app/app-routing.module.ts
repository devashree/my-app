import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService } from '../services/auth-guard.service';
import { AuthGuard } from './auth-guard/auth.guard';

const routes: Routes=[
  {
    path:'register',
    loadChildren: '../app/register/register.module#RegisterModule'
  },
  {
    path: 'signin',
    loadChildren: '../app/signin/signin.module#SigninModule'
  },
  {
    path: 'home',
    loadChildren: '../app/home/home.module#HomeModule',
    canActivate: [AuthGuard],
  },
  {
    path: 'userdetails',
    loadChildren: '../app/userdetails/userdetails.module#UserDetailsModule',
  },
  {
    path: 'filter',
    loadChildren: '../app/filter/filter.module#FilterModule',
  },
  {
    path: '',
    redirectTo: '/signin',
    pathMatch: 'full',
    canActivate: [AuthGuard]
  }
]
export const routing=RouterModule.forRoot(routes,{useHash:false});
@NgModule({
  imports: [
    routing
  ],
  exports: [ RouterModule],
  declarations: []
})
export class AppRoutingModule { }
