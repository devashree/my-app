import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { UserService} from '../services/user.service';
import { HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { RegisterService} from '../services/register.service';
import { AlertsModule} from 'angular-alert-module';
import { AuthGuardService} from '../services/auth-guard.service';
import { AuthGuard} from './auth-guard/auth.guard';


@NgModule({
  
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    AlertsModule.forRoot()
  ],
  
  providers: [UserService, RegisterService,AuthGuard,AuthGuardService],
  bootstrap: [AppComponent]
})

export class AppModule { }
 