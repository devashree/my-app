// import { Injectable } from '@angular/core';

// import { Observable,of } from 'rxjs';
// import { tap, delay } from 'rxjs/operators';

// @Injectable({
//   providedIn: 'root'
// })
// export class AuthGuard  {
 
//   isLoggedIn: boolean=false;

//   redirectUrl: string;

//   login():Observable<boolean> {
//     return of(true).pipe(
//       delay(1000),
//       tap(val=>this.isLoggedIn=true)
//     );
//   }
//   logout():void{
//     this.isLoggedIn=false;
//   }
// }

import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthGuardService } from '../../services/auth-guard.service';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthGuardService,
    private myRoute: Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if(this.auth.isLoggednIn()){
        return true;
      }else{
        this.myRoute.navigate(["signin"]);
        return false;
      }
  }
}