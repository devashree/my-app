import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegisterService } from '../../services/register.service';
import { Register} from '../../models/register';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {

  constructor(public route: ActivatedRoute,
              public registerService: RegisterService) { }

  name: string;
  searchedData: any;
  registerData: Register[];
  ngOnInit() {
    this.route.params.subscribe(params=>{
      this.searchedData=params['data'];
    })
  }
}
