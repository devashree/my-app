import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FilterComponent} from '../filter/filter.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule} from '@angular/forms';

const routes: Routes = [
    {
      path: '',
      component: FilterComponent
    }
  ];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: [FilterComponent]
})
export class FilterModule { }