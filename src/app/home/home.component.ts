import { Component, OnInit } from '@angular/core';
import { AuthGuardService } from '../../services/auth-guard.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(public auth: AuthGuardService, public router: Router) { }

  ngOnInit() {
  }

  /**
   * @description This function logouts the session
   */
  logout(){
    localStorage.removeItem("LoggedInUser");
     this.router.navigate(['/signin']);
  }

  /**
   * @description This function navigates user to user details page
   */
  users(){
     if(this.auth.isLoggednIn()){
      this.router.navigate(['/userdetails']);
     }
  }

}
