import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent} from '../home/home.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule} from '@angular/forms';

const routes: Routes = [
    {
      path: '',
      component: HomeComponent
    }
  ];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: [HomeComponent]
})
export class HomeModule { }