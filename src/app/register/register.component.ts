import { Component, OnInit } from '@angular/core';
import { environment} from '../../environments/environment';
import { RegisterService} from '../../services/register.service';
import { Register } from '../../models/register';
import { Router,ActivatedRoute} from '@angular/router';
import { CountService } from '../../services/count.service';
import { AlertsService} from 'angular-alert-module';
import { count } from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  
  registerData: Register[];
  submit: boolean=false;
  message: string="Please Fill Company Contact number proerly";
  constructor(public registerService: RegisterService,
              public router: Router,
              public route: ActivatedRoute,
              public alert: AlertsService,
              public count: CountService) { }
  api=environment.apiUrl;
  countTemp=0;
  ngOnInit() {
    this.getAllUsers();
    console.log(this.register.count);
  }
  /**
   * @description This array is to get all modules dynamically to html
   */
  modules=[

    'DashBoard',
    'Calender',
    'Discussion',
    'MailBox',
    'Client',
    'Vendor',
    'Task',
    'Sales',
    'Support',
    'Accounting',
    'HR',
    'Report',
    'Utilities',
    'Survey'
  ]

  /**
   * @description register is the object where data is stored fetched from form input
   */
  register={
    fullname: '',
    username: '',
    email:'',
    companyname:'',
    companyemail:'',
    companydomain:'',
    companycontact:'',
    password:'',
    module:'',
    count:null,
    confirmpassword:'',
  }

  /**
   * @description This function is called on submit button
   */

  onSubmit(){
    this.signup();
    console.log("Module",this.register.module);
   // this.submit=true;
  }
  getValue(event: any,val:number){
    console.log(event.target.value,val);
    this.register.module=event.target.value;
  }
  /**
   * @description This function is called for contact number validation
   */
  signup(){
    if(this.register.password===this.register.confirmpassword)
    {
      this.countTemp+=1;
      this.register.count=this.countTemp;
     // this.register.count+=1;
      console.log(this.register.count);
     
      this.addNewUser();
     // this.register.count=this.countTemp+1;
     // countTemp=this.register.count+1;
     
    
      //this.count.setCount();
      this.router.navigate(['/signin',{count: this.register.count}]);
    }  else
    {
      console.log("Please enter password and confirm password ");
    }
  }
  /**
   * @description This function is to add new user where data will passed as a body using http post
   */
  addNewUser(){
     this.registerService.addNewUser(this.register).subscribe(response=>{
       console.log("Response",response);
     },err=>{
       console.log(err);
     })
  }

  /**
   * @description This function is to get all the users by using http get method
   */
  getAllUsers(){
    this.registerService.getRegisteredUser().subscribe(data=>{
      this.registerData=data;
      console.log(this.registerData);
    })
  }
}