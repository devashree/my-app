import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RegisterComponent} from '../register/register.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule} from '@angular/forms';

const routes: Routes = [
    {
      path: '',
      component: RegisterComponent
    }
  ];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: [RegisterComponent]
})
export class RegisterModule { }