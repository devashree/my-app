import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService} from '../../services/auth-guard.service';
import { AuthGuard} from '../auth-guard/auth.guard';
import { SigninComponent } from './signin.component';


const routes: Routes = [
  {
    path: '/signin',
    component: SigninComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers:[AuthGuard,AuthGuardService]
})
export class SigninRoutingModule { }