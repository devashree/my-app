import { Component, OnInit } from '@angular/core';
import { UserService} from '../../services/user.service';
import { Login} from '../../models/login';
import { Register} from '../../models/register';
import { RegisterService } from '../../services/register.service';
import { Router,ActivatedRoute} from '@angular/router';
import { AuthGuardService} from '../../services/auth-guard.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  constructor(public userService: UserService,
              public registerService: RegisterService,
              public router: Router,
              public authGuard: AuthGuardService,
              public route: ActivatedRoute,
           ) { }
  submit: boolean=false;
  hideLogin: boolean=false;
  loginData: Login[];
  registerData: Register[];

  
  /**
   * @param username username from form as input
   * @param password password from form as input 
   * @param contactnumber static number given in model
   * @description model is the object to store data from form and send as body
   */
  model={
    username:'',
    password:'',
    contactnumber: 9921488966
  }
  
  /**
   * @description This function is called when form is submitted
   */
  onSubmit(){
   this.submit=true;
   this.addUser();
  }

  /**
   * @description This function is called after form is submitted 
   */
  register(){
     this.router.navigate(['/register']);
  }

  /**
   * @description This function will get user data by using http get method
   */
  ngOnInit() {
    this.userService.getUser().subscribe(data=>{
      this.loginData=data;
      console.log("Fetched data",this.loginData);
    })
    this.registerService.getRegisteredUser().subscribe(data=>{
      this.registerData=data;
    })
    let val=localStorage.getItem("LoggedInUser");
    if(this.authGuard.isLoggednIn()){
      this.router.navigate(['/home']);
    }
  }

  /**
   * @description This function is to add data by using http post method
   */
  addUser(){
    this.userService.addUser(this.model).subscribe(response=>{
      if(response){
        console.log("Response",response);
        this.router.navigate(['/home']);
      }else{
        console.error();
      }
    })
  }


  /**
   * @description this function redirects page to home page if login is correct
   */

  goToHomePage(){
    let name,password,registerLen;
    name=this.model.username;
    password=this.model.password;
    registerLen=this.registerData.length;
    for(let i=0;i<registerLen;i++){
      console.log("registerdata",this.registerData[i].email,this.registerData[i].password);
      console.log(name,password);
      if((name===this.registerData[i].email) && (password===this.registerData[i].password)){
        console.log("Sign in successfull");
        this.authGuard.sendToken(name);
        this.router.navigate(['/home']);
      }else{
        console.log("Not successfull");
      }// else 
    }// for loop
  }// function
}