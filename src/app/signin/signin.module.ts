import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SigninComponent} from '../signin/signin.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule} from '@angular/forms';

const routes: Routes = [
    {
      path: '',
      component: SigninComponent
    }
  ];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: [SigninComponent]
})
export class SigninModule { }