import { NgModule} from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UserDetailsComponent } from './userdetails.component';
const routes: Routes = [
 
  {
    path: '/userdetails',
    component: UserDetailsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }