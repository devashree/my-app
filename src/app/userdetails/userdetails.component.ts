import { Component, OnInit,Output,Input,EventEmitter } from '@angular/core';

import { RegisterService } from '../../services/register.service';
import { Register } from '../../models/register';
import { Router,ActivatedRoute} from '@angular/router';
import { HttpClient} from '@angular/common/http';
import { AuthGuardService } from '../../services/auth-guard.service';
import { Subject } from 'rxjs';
import { CountService } from '../../services/count.service';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-userdetails',
  templateUrl: './userdetails.component.html',
  styleUrls: ['./userdetails.component.scss']
})
export class UserDetailsComponent implements OnInit {
 // @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();
 @Output() pageChange: EventEmitter<number>;
 
 @Input() id: string;
@Input() maxSize: number;
  constructor(public registerService: RegisterService,
              public auth: AuthGuardService,
              public router: Router,
              public route: ActivatedRoute,
              public count: CountService,
              public http: HttpClient) { }
  registerData: Register[]=[];
  event: any;
  len:number;
  dtOptions: DataTables.Settings={};
  dtTrigger: Subject<any> = new Subject();

  getStatus: boolean=false;
  sortValue: string;
  showShort: boolean=false;
  username: string;
  setStatus: boolean=false;
  items: any[]=[];
  getCurrentPage: number;
  itemsCount: number;
  searchedData: Object;
  searchValue: string='';
  showSearch: boolean=false;
  ngOnInit() {
    this.getUserCount();
    this.getUsers(this.event);
    this.pageChanged(1,1);
    this.dtOptions={
      pagingType: 'full_numbers',
      pageLength: 2
     
     
   
      // columns:[
      //   {
      //     title:'ID',
      //     data:'id'
      //   },
      //   {
      //     title:'USERNAME',
      //     data:'username'
      //   },
      //   {
      //     title:'FULLNAME',
      //     data:'fullname'
      //   },
      //   {
      //     title:'TITLE',
      //     data: 'title'
      //   }
      // ]

    };
    // this.http.get<Register[]>(environment.registerUrl)
    // .map(this.extractData)
    // .subscribe(data => {
    //   this.registerData = data;
    //   this.dtTrigger.next();
    // });
  }
  ngOnDestroy(): void {
    // Do not forget to unsubscribe the event
    this.dtTrigger.unsubscribe();
  }

  private extractData(res: Response) {
    const body = res.json();
    return body || {};
  }

  getUserCount(){
    this.registerService.getUserCount().subscribe(data=>{
      this.itemsCount=data['count'];
    })
  }
  /**
   * 
   * @param count page changed count
   * @description This function get all registered users
   */
  getUsers(count:number){
    this.registerService.getRegisteredUser(count)
    .subscribe(data=>{
      this.registerData=data;
      this.dtTrigger.next();
      console.log("Items",this.items);
      })
  }
  /**
   * 
   * @param event this param will get page changed value
   * @description This function will get value of pagination changed value
   */
  pageChanged(event,getCurrentPage,getTotalItems?)
  {
    this.event=event;
    if(event===1){
      this.getStatus=false;
      this.setStatus=true;
    }
    else if(event>=1){
      this.getStatus=true;
      this.setStatus=false;
    }else{
      this.getStatus=false;
      this.setStatus=true;
    }
      if(this.registerData.length!=0){
        this.getUsers(event);
      }
    }

  /**
   * @description Navigates user to home page 
   */
  home(){
    if(this.auth.isLoggednIn()){
      this.router.navigate(['/home']);
    }
  }

  /**
   * @description This function navigates user for new registeration 
   */
  register(){
    this.router.navigate(['/register']);
  }
  /**
   * 
   * @param id id param is used to search the particular user
   * @description This function searches data by Id
   */
  searchById(value){
    let val=value.toLowerCase();

    return this.items.filter(item=>{
      return item.name.toLowerCase().includes(val);
    })
    
  }

  /**
   * 
   * @param value Passing value to sort data
   * @description This function is to sort data according to value 
   */
  sort(value){
    if(value==='module'){
      this.sortValue='module';
    }else{
      this.sortValue='username';
    }
  }
}
