import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserDetailsComponent } from '../userdetails/userdetails.component';
import { Routes, RouterModule } from '@angular/router';
import { NgxPaginationModule } from 'ngx-pagination';
import { FormsModule} from '@angular/forms';
import { OrderModule} from 'ngx-order-pipe';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { DataTablesModule} from 'angular-datatables';


const routes: Routes = [
    {
      path: '',
      component: UserDetailsComponent
    }
  ];

@NgModule({
  imports: [
    CommonModule,
    OrderModule,
    NgxPaginationModule,
    Ng2SearchPipeModule,
    DataTablesModule,
    FormsModule,
    RouterModule.forChild(routes)
  ],
  exports:[RouterModule],
  declarations: [UserDetailsComponent]
})
export class UserDetailsModule { }