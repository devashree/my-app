export const environment = {
  production: false,

  /**
   * @description api for users logged in
   */
  apiUrl :'http://localhost:3000/api/logins',

  /**
   * @description api for registered users
   */
  registerUrl: 'http://localhost:3000/api/demo',

  /**
   * @description api for getting count
   * 
   */

   countUrl: 'http://localhost:3000/api/demo/count'

  

};
