// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  /**
   * @description api for users logged in
   */
  apiUrl : 'http://localhost:3000/api/logins',
  /**
   * @description api for registered users
   */
  registerUrl: 'http://localhost:3000/api/demo',

   /**
   * @description api for getting count
   */

  countUrl: 'http://localhost:3000/api/demo/count'
};

/*
 * In development mode, for easier debugging, you can ignore zone related error
 * stack frames such as `zone.run`/`zoneDelegate.invokeTask` by importing the
 * below file. Don't forget to comment it out in production mode
 * because it will have a performance impact when errors are thrown
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
