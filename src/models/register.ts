export interface Register{
    fullName: string;
    username: string;
    email: string;
    companyName: string;
    companyEmail: string;
    companyDomain: string;
    companyContact: number;
    module: string;
    password: string;
    count:number;
    confirmPassword: string;
}