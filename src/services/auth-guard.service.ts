// import { Injectable } from '@angular/core';
// import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,Router } from '@angular/router';
// import { AuthGuard} from '../app/auth-guard/auth.service';
// @Injectable({
//   providedIn: 'root'
// })
// export class AuthGuardService implements CanActivate {

//   constructor(public router: Router,public authService: AuthGuard) { }
//   canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):boolean {
//     let url: string = state.url;

//     return this.checkLogin(url);
//   }
//   checkLogin(url: string): boolean {
//     if (this.authService.isLoggedIn) { return true; }

//     // URL for redirecting
//     this.authService.redirectUrl = url;

//     // Navigate to the login page 
//     //this.router.navigate(['/login']);
//     return false;
//   }
//   canActivateChild(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
//     return this.canActivate(route, state);
//   }

// }

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
@Injectable()
export class AuthGuardService {
  constructor(private myRoute: Router) { }

  /**
   * 
   * @param email email param is email id of user who have logged in to store in local storage
   */
  sendToken(email: string) {
    localStorage.setItem("LoggedInUser", email);
  }

  /**
   * @description This function is to getitem from local storage
   */
  getToken() {
    return localStorage.getItem("LoggedInUser");
  }

  /**
   * @description This function returns true or false if user has already logged in.
   */
  isLoggednIn() {
    return this.getToken() !== null;
  }

  /**
   * @description This function is to close the session
   */
  logout() {
    return localStorage.removeItem("LoggedInUser");
  }
}
