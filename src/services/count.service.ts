import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CountService {

  constructor(
    public countService: CountService
  ) { }
  static count: number=0;
  
  /**
   * @description This function is to set count when user registers  
   */
  setCount(){
    CountService.count+=1;
  }

  /**
   * @description This function returns count ie number of users.
   */
  getCount(){
    return  CountService.count;
  }
}
