import { Injectable } from '@angular/core';
import { HttpClient,HttpParams} from '@angular/common/http';
import { Register} from '../models/register';
import { Observable } from 'rxjs';
import { environment} from '../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {
 
  constructor(public http: HttpClient) { }
  apiUrl=environment.registerUrl;
  countUrl=environment.countUrl;
  offset:number=0;

  /**
   * 
   * @param count This is optional param to get data according to page count
   * @description This function retrieves users data by API call
   */
  getRegisteredUser(count?:number): Observable<Register[]>{
    if(count>1){
      
      this.offset+=5;
    }
    else {
      this.offset=0;
    }
    let params=new HttpParams()
    .set('filter','{"limit":5,"offset":'+this.offset+'}');
    return this.http.get<Register[]>(this.apiUrl,{ params});
  }

  /**
   * 
   * @param body body param will add data to api when new user is added
   * @description This function adds new user
   */
  addNewUser(body: any){
    return this.http.post(this.apiUrl,body);
  }
  
  /**
   * 
   * @param name name param is to search user details
   * @description This function is to find data according to name
   */
  searchByName(name): Observable<Register[]>{
    let params=new HttpParams()
    .set('fullname', name);
    return this.http.get<Register[]>(this.apiUrl+'/',{params});
  }

  /**
   * 
   * @param id to get data according to id
   * @description This function is to get user details according user id 
   */
  getDataById(id): Observable<Register[]>{
    let query= this.http.get<Register[]>(this.apiUrl+'/'+id);
    console.log(query);
    return query;
  }

  /**
   * @description This function is to get count of total number of users
   */
  getUserCount(){
    return this.http.get(this.countUrl);
  }
  
}
