import { Injectable } from '@angular/core';
import { HttpClient,HttpParams} from '@angular/common/http';
import { environment} from '../environments/environment';

import {Register} from '../models/register';
import {Login} from '../models/login';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  //This is a user api url from environment file
  apiUrl=environment.apiUrl;
  constructor(public http: HttpClient) { }
  
  /**
   * @description This function is to get all users who have logged in
   */
  public getUser(): Observable<Login[]>{
    return this.http.get<any>(this.apiUrl);
  }

  /**
   * 
   * @param body body param is data added an api call
   * @description This function adds logged in user
   */
  public addUser(body: any){
    return this.http.post(this.apiUrl,body);
  }
}
